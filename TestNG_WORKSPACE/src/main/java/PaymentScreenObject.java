import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class PaymentScreenObject {
    private static final SelenideElement ORDER_BUTTON = $("[id=popup-checkout]");
    private static final SelenideElement NAMESURENAME_FIELDS = $("[id=reciever_name]");
    private static final SelenideElement CITIES_DDM = $("[name=suggest_locality]"); // open dropdown
    private static final SelenideElement SELECT_CITIES_DDM = $("[text=Львов]");//select value
    private static final SelenideElement MOB_FIELD = $("[id=reciever_phone]");
    private static final SelenideElement EMAIL_FIELD = $("[id=reciever_email]");


    public PaymentScreenObject openPaymentScreen() {
        ORDER_BUTTON.click();
        return this;
    }

    public PaymentScreenObject nameSurname(String value) {
        NAMESURENAME_FIELDS.setValue(value);
        return this;
    }


    public PaymentScreenObject selectCities() {
        CITIES_DDM.click();
        return this;
    }


    public PaymentScreenObject selectCity() {
        SELECT_CITIES_DDM.click();
        return this;

    }


    public PaymentScreenObject enterMobTel(String value) {
        MOB_FIELD.setValue(value);
        return this;
    }


    public PaymentScreenObject enterEmail(String value) {
        EMAIL_FIELD.setValue(value);
        return this;
    }
}
