import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Selenide.*;


public class RozetkaSearchPageObject {


    private static final SelenideElement SEARCH_FP = $("[name=search]");
    private static final SelenideElement SEARCH_BTN = $("[type=submit]");
    private static final SelenideElement BUY_BBTN = $("[name=topurchases]");
    private static final SelenideElement CHECK_ITEM = $("[class=cart-i-title]");
    private static final ElementsCollection SEARCH_MFP = $$("[name=price]");

    public RozetkaSearchPageObject openPage() {
        open("https://rozetka.com.ua");
        return this;

    }

    public RozetkaSearchPageObject searchItem(String value) {
        SEARCH_FP.setValue(value);
        return this;
    }


    public RozetkaSearchPageObject clickSearchButton() {
        SEARCH_BTN.pressEnter();
        return this;
    }

    public RozetkaSearchPageObject verifyAvailability(String value) {
        CHECK_ITEM.shouldHave(Condition.text(value));

        return this;
    }

    public RozetkaSearchPageObject clickBuy() {
        BUY_BBTN.click();
        return this;
    }

    public RozetkaSearchPageObject amountOfResults(int a) {
        SEARCH_MFP.shouldHave(size(a));
        return this;
    }

    public RozetkaSearchPageObject waiter(long d, int b) throws InterruptedException {
        wait(d, b);
        return this;
    }

}

