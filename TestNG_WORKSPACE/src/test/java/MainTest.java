import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class MainTest {

    private RozetkaSearchPageObject rozetkaSearchPageObject = new RozetkaSearchPageObject();
    private PaymentScreenObject paymentScreenObject = new PaymentScreenObject();

    @BeforeTest
    public void setUpBrowser() {
        Configuration.browser = "chrome";
    }



    @Test
    public void testMethod() {

        rozetkaSearchPageObject.
                openPage().
                searchItem("Вся фигня - от мозга?! Простая психосоматика для сложных граждан").
                clickSearchButton().
                clickBuy().verifyAvailability("Вся фигня - от мозга?! Простая психосоматика для сложных граждан");


        paymentScreenObject.
                openPaymentScreen().
                nameSurname("dfsdfsdf").
                selectCities().
                selectCity().
                enterEmail("igork750@gmail.com").
                enterMobTel("123123123");


    }

    @Test
    public void testMethod2() {
        rozetkaSearchPageObject.
                openPage().
                searchItem("iPhone X").
                clickSearchButton().
                amountOfResults(5);
    }
}
